public class Card {
	
	private int rank;
	private String suit;
	
	public Card(int rank, String suit) {
		this.rank = rank;
		this.suit = suit;
	}
	
	public int getRank() {
		return this.rank;
	}
	
	public String getSuit() {
		return this.suit;
	}
	
	public String toString() {
		if (rank == 1) {
			return "Ace of " + suit;
		} else if (rank == 11) {
			return "Jack of " + suit;
		} else if (rank == 12) {
			return "Queen of " + suit;
		} else if (rank == 13) {
			return "King of " + suit;
		} else {
			return rank + " of " + suit;
		}
	}
	
	public double calculateScore() {
		
		double score = rank;
		
		if (this.suit.equals("hearts")) {
			score += 0.4;
		} else if (this.suit.equals("spades")) {
			score += 0.3;
		} else if (this.suit.equals("diamonds")) {
			score += 0.2;
		} else if (this.suit.equals("clubs")) {
			score += 0.1;
		}
		return score;
	}
}
