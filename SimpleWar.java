public class SimpleWar {
	
	public static void main(String[] args){
		
		Deck deck = new Deck();
		deck.shuffle();
		
		double player1Points = 0.0;
		double player2Points = 0.0;
		
		while(deck.length() > 2) {
			Card playerOne = deck.drawTopCard();
			Card playerTwo = deck.drawTopCard();
			
			System.out.println("Player 1's card: " + playerOne);
			player1Points = playerOne.calculateScore();
			System.out.println("Player 1's points: " + player1Points);
			
			System.out.println("Player 2's card: " + playerTwo);
			player2Points = playerTwo.calculateScore();
			System.out.println("Player 2's points: " + player2Points + "\n");
			
			if (player1Points > player2Points) {
				System.out.println("Player 1 wins the round\n");
			} else {
				System.out.println("Player 2 wins the round\n");
			}
		}
		
		if (player1Points > player2Points) {
			System.out.println("Congratulations Player 1! You Win The Game!!");
		} else {
			System.out.println("You lose the game Player 1! Better luck next time!");
		}
	}
	
}